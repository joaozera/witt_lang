type terminal_type = X

type arithmeticOperator =
  | Sum
  | Diff
  | Mult 
(*//| Div //deixa o div de fora por ora*)

type arithmeticExpression =
 | Num of int
 | AE of
     {
       left : arithmeticExpression;
       oper : arithmeticOperator;
       right: arithmeticExpression
     };;


type unarySymbol_type = One

type unaryNumber_type = unarySymbol_type list

type exponent_type = 
 | AEs of arithmeticExpression
 | UN of unaryNumber_type

type operation_type =
  {
    l : exponent_type;
    symbol : string;
    r : exponent_type
  };;

type application_type = Application

(* expression é uma árvore de operações *)
type expression_type =
    | Op of operation_type
    | App of application_type
    | Term of terminal_type
