
(* Definição do tipo árvore *)

type 'a binary_tree =
  | Leaf of 'a
  | Tree of 'a * 'a binary_tree * 'a binary_tree;;

class virtual ['a] tree root =
object (self)

  val mutable my_tree : 'a binary_tree = root


         (*method insert x s = Tree (x, Leaf, s)*)
                      
                      
  val mutable current_tree = root

  method get_tree : 'a binary_tree = my_tree
  
  method set_tree (receivedTree : 'a binary_tree) = 
    my_tree <- receivedTree;
   
                      
  method get_node =
    match current_tree with
    | Leaf folha -> folha
    | Tree (node, left, right) -> node

             
  method set_current_left  =
    match current_tree with
    | Leaf folha -> Leaf folha
    | Tree (node, left, right) -> left
             
  method go_left =
    current_tree <- self#set_current_left;

             
  method set_current_right : 'a binary_tree =
    match current_tree with
    | Leaf folha -> Leaf folha
    | Tree (left, node, right) -> right

  method go_right =
    current_tree <- self#set_current_right;


  method virtual print : unit

             (*
  method set_node (a_node) =
    match my_tree with
    | Leaf folha -> a_node
    | Tree (left, node, right) -> Tree(left, a_node, right)
    | Empty -> a_node
              *)
                 (*method virtual set_node : 'a -> unit*)
       (*method get_symbol = symbol

       method set_symbol (receivedSymbol : char) = 
  	 symbol <- receivedSymbol;
         
       method to_string = Printf.sprintf "%c" symbol*)  
end


let it be = "paul mccartney"
              
        (*
class expression_tree =
object(self)
  inherit [int] tree

  method set_node (a_node) : 'a binary_tree =
    match a_node with
    | Leaf folha -> a_node
    | Tree (left, node, right) -> a_node
    | Empty -> a_node
        
end;;
         *)
