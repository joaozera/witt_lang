open Types;;
open Operation;;
open Application;;
open Terminal;;

(*

class expression (a_expression : expression_type) =
object (self)
     
  val mutable my_expression = a_expression

  method get_expression : expression_type = my_expression

  method set_expression (received_expression : expression_type) =
    my_expression <- received_expression;                     
      
end

 *)

class expression =
object (self)
  inherit operation as super_operation
  inherit application as super_application
  inherit terminal as super_terminal
     
  method string_of_expression (my_expression : expression_type) =
    match my_expression with
    | Op operation -> super_operation#string_of_operation (operation)
    | App application -> super_application#string_of_application (application)
    | Term terminal -> super_terminal#string_of_terminal (terminal)
        
end
 
