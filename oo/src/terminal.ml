open Types;;

(*
class terminal (a_symbol : char) =
object (self)
  val mutable symbol : char = a_symbol
             
  method get_symbol = symbol

  method set_symbol (receivedSymbol : char) = 
  	symbol <- receivedSymbol;
                    
  method to_string = Printf.sprintf "%c" symbol      
end
 *)

class terminal =
object(self)
  method string_of_terminal (a_terminal : terminal_type) = "X"
end
