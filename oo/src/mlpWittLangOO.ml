(*

Trabalho Final da Disciplina de Modelos de Linguagens de Programação

Ciência da Computação - UFRGS - 2017/2

Alunos:
- João Marcos Flach - 242249
- Renan Luigi Martins Guarese - 193410

Interpretador da Linguagem da Teoria Geral das Operações

Implementação Orientada a Objetos

 *)

open Types;;
open Expression;;
open Terminal;;
open Operation;;
open Tree;;
open Int_tree;;
open Expression_tree;;

(*
let my_expression = new terminal 'a';;


Printf.printf "%s\n" my_expression#to_string;;
Printf.printf "%c\n" my_expression#get_symbol;;

my_expression#set_symbol('b');;

Printf.printf "%s\n" my_expression#to_string;;
Printf.printf "%c\n" my_expression#get_symbol;;



let my_tree : 'a binary_tree = Tree (5, Leaf 3, Leaf 4);;


let my_folha = Leaf 2;;
let myArv = new int_tree my_folha;;

myArv#set_tree my_tree;;

myArv#print;;
 *)

let um = [One];;
let dois = [One; One];;
let tres = [One; One; One];;
let quatro = [One; One; One; One];;
let cinco = [One; One; One; One; One];;
let seis = [One; One; One; One; One; One];;
let sete = [One; One; One; One; One; One; One];;
let oito = [One; One; One; One; One; One; One; One];;
let nove = [One; One; One; One; One; One; One; One; One];;
let dez = [One; One; One; One; One; One; One; One; One; One];;

let my_expression_tree = new expression_tree (Tree (App Application, Leaf(Term X), Leaf(Term X)));;

let my_expression_tree_2 = new expression_tree (Tree (App Application, Tree(App Application, Leaf(Term X), Leaf(Term X)), Leaf(Term X)));;

let my_expression_tree_3 = new expression_tree (Tree
                                                  (App Application,
                                                   Tree
                                                     (App Application,
                                                      Leaf(Op {l= UN dois; symbol= "O"; r = UN [One]}),
                                                      Leaf(Term X)),
                                                   Leaf(Term X)));;

  (*
my_expression_tree#print;;

my_expression_tree_2#print;;

my_expression_tree_3#print;;
   *)




(* DEMO *)
  
  
let my_multiplication = new expression_tree (Tree
                                          (App Application,
                                           Leaf(Op {l= UN sete; symbol= "O"; r = UN oito}),
                                           Leaf(Term X))
                                          );;

my_multiplication#print;;

let my_multiplication_expanded = new expression_tree my_multiplication#expand;;

  (* Printf.printf "\n\nÁrvore explodida:\n\n";; *)

my_multiplication_expanded#print;;

let my_multiplication_imploded = new expression_tree my_multiplication_expanded#implode;;

  (* Printf.printf "\n\nÁrvore implodida:\n\n";; *)

my_multiplication_imploded#print;;

(*let my_tree2 = new tree 3;;
*)
