open Types;;
open UnarySymbol;;

class unaryNumber =
object (self)
  inherit unarySymbol as super_us

  method string_of_un (a_un : unaryNumber_type) =
    let rec string_of_un_rec x =
      match x with
      | [] -> ""
      | h::t -> super_us#string_of_us(h) ^ string_of_un_rec(t) 
    in string_of_un_rec(a_un)
end
