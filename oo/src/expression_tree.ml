open Tree;;
open Expression;;
open Types;;

class expression_tree root =
object (self)
  inherit [expression_type] tree root
  inherit expression as super_expression
        
  method to_string : string=
    let rec to_string_rec x =
      match x with
      | Leaf folha -> "[" ^ super_expression#string_of_expression (folha) ^ "]"
      | Tree (node, left, right) -> "[" ^ super_expression#string_of_expression (node) ^ " " ^ to_string_rec(left) ^ " " ^ to_string_rec(right) ^ "]"
    in to_string_rec(my_tree);

  method print = Printf.printf "%s\n" self#to_string


  method expand_left_exponent (my_expression : operation_type) = 
    let rec expand_left_exponent_rec (x) =
      match x.l with
      | AEs aes -> Leaf(Op x)
      | UN unary ->  match unary with
                     | [] -> Leaf(Op x)
                     | [y] -> Leaf(Op x)
                     | head :: tail ->
                        Tree(
                            App Application,
                            expand_left_exponent_rec({r = x.r; symbol = x.symbol; l =  UN [head]}),
                            expand_left_exponent_rec({r = x.r; symbol = x.symbol; l =  UN tail}))
    in expand_left_exponent_rec (my_expression);

       
  method expand_right_exponent (my_expression : operation_type) =
    let rec expand_right_exponent_rec (x) =
      match x.r with
      | AEs aes -> Leaf(Op x)
      | UN unary ->  match unary with
                     | [] -> self#expand_left_exponent x
                     | [y] -> self#expand_left_exponent x
                     | head :: tail ->
                        Tree(
                            App Application,
                            expand_right_exponent_rec({l = x.l; symbol = x.symbol; r =  UN [head]}),
                            expand_right_exponent_rec({l = x.l; symbol = x.symbol; r =  UN tail}))
    in expand_right_exponent_rec (my_expression)
                                     
  method expand = 
    let rec tree_expand_rec(a_tree) =
      match a_tree with
      | Tree (node, left, right) -> Tree(node, tree_expand_rec(left), tree_expand_rec(right))
      | Leaf a ->
         match a with
         | App a -> Leaf (App a)
         | Term t -> Leaf (Term t)
         | Op o -> self#expand_right_exponent o
    in tree_expand_rec(my_tree);


  method implode =
    let rec implode_rec (a_tree) =
      match a_tree with
      | Leaf a -> Leaf a
      | Tree (node, left, right) ->
         match node with
         | Term t -> Leaf (Term t)
         | Op o -> Leaf (Op o)
         | App a ->
            match right with
            | Leaf a -> Tree (node, implode_rec (left), right)
            | Tree (right_node, right_left, right_right) ->
               match right_node with
                 | Term t -> Leaf (Term t)
                 | Op o -> Leaf (Op o)
                 | App a -> implode_rec(
                                Tree(
                                App Application,
                                Tree(
                                    App Application,
                                    implode_rec left,
                                    implode_rec right_left
                                  ),
                                implode_rec right_right))
    in implode_rec(my_tree);
       
end;;
