open Types;;
open UnaryNumber;;

class exponent =
object (self)
  inherit unaryNumber as super_un

  method string_of_exponent (a_exponent : exponent_type) =
    match a_exponent with
    | AEs ae -> "lala"
    | UN un -> super_un#string_of_un(un)

end
