type operation_term_symbol = Omega | Identity ;;
type numeral_symbol = Zero | S ;;
type arithmetical_symbol = Add | Mult ;;
type equation_symbol = Equals ;;
type operation_expression_symbol = Psi | X ;;

let operation_term_symbol_to_string a_symbol =
  match a_symbol with
  | Omega -> "O"
  | Identity -> "I";;
    
let numeral_symbol_to_string a_symbol =
  match a_symbol with
  | Zero -> "0"
  | S -> "S";;

let arithmetical_symbol_to_string a_symbol =
  match a_symbol with
  | Add -> "+"
  | Mult -> "x";;

let equation_symbol_to_string a_symbol =
  match a_symbol with
  | Equals -> "=";;

let operation_expression_to_string a_symbol =
  match a_symbol with
  | Psi -> "E"
  | X -> "x";;
