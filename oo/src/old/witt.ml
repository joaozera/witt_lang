open Char;;
open List;;
open Printf;;
open Types;;

let rec numeral_list2string a_list =
  match a_list with
  | [] -> ""
  | h::t -> numeral_symbol_to_string h ^ numeral_list2string t;;

let rec list_numeral_symbol_to_string a_list =
  match a_list with
  | [] -> ""
  | h::t -> numeral_symbol_to_string h ^ list_numeral_symbol_to_string t;;

let print_numeral_list e =
  print_endline (numeral_list2string e);;
  
class numeral (a_number : int) =
object (self)
  val arabic = a_number
  val mutable symbol_list = ( [] : numeral_symbol list)

  method get_arabic = arabic
  method get_symbol_list = symbol_list
                        
  method to_symbol_list =
    for i=1 to arabic do
      symbol_list <- append symbol_list [S]
    done;
      symbol_list <- append symbol_list [Zero]
      
  method print_arabic = print_int self#get_arabic

  method print_symbol_list = print_numeral_list self#get_symbol_list

  method to_string = list_numeral_symbol_to_string self#get_symbol_list
end

class operation (a_symbol : operation_term_symbol) (a_numeral : numeral) =
object (self)
  val symbol = a_symbol
  val numeral = a_numeral
  method to_string =
    operation_term_symbol_to_string a_symbol ^ "^" ^ "{" ^ a_numeral#to_string ^ "}"
end

class high_order_operation (a_operation : operation) (a_numeral : numeral) =
object (self)
  val operation = a_operation
  val numeral = a_numeral
  method to_string =  
    "(" ^ a_operation#to_string ^ ")" ^ "^" ^ "{" ^ a_numeral#to_string ^ "}" 
end
  
(*
type symbol = Omega | Add | Mult | Zero | S | App ;;

type expoent =
  | Arabic of int
  | Symbol_list of char list;;

type operation =
  | Symbol of string
  | Op_list of operation list
  | Operation_exp of operation * expoent;;

type witt_num = { op : operation ; var : char };;



  
let rec to_string_char_list a_list =
  match a_list with
  | [] -> ""
  | h::t -> escaped(h) ^ to_string_char_list t;;
  
let to_string_exp a_expoent =
  match a_expoent with
  | Arabic (numero) -> string_of_int(numero)
  | Symbol_list (a_symbol_list) -> to_string_char_list a_symbol_list;;


  
let rec to_string a_operation =
  match a_operation with
  | Symbol (simbolo) -> simbolo
  | Op_list lista -> to_string_lst lista
  | Operation_exp (op, exp) ->
     "(" ^ to_string op ^ ")"  ^ "^{" ^ to_string_exp exp ^ "}"
and to_string_lst a_list =
  match a_list with
  | [] -> ""
  | h::t -> to_string h ^ "'" ^ " " ^ to_string_lst t
;;

let print_expr e =
  print_endline (to_string e);;

class number (a_number : int) =
object
  val arabic = a_number
  val mutable witt_def = "O{0}"
  method get_num = arabic
  method get_witt = witt_def
  method to_witt_def =
    for i=0 to arabic do
      witt_def <- "S" ^ witt_def
    done
end;;

 *)
