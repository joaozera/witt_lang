open Tree;;

(* 11 linhas de código que levaram mais de 2h (mas ficou elegantão) *)

class int_tree root =
object (self)
  inherit [int] tree root
        
  method to_string : string=
    let rec to_string_rec x =
      match x with
      | Leaf folha -> "[" ^ string_of_int (folha) ^ "]"
      | Tree (node, left, right) -> "[" ^ string_of_int (node) ^ " " ^ to_string_rec(left) ^ " " ^ to_string_rec(right) ^ "]"
    in to_string_rec(my_tree);

  method print = Printf.printf "%s\n" self#to_string
               
end;;
