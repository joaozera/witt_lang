open Types;;
open Exponent;;

(*
class operation (a_symbol : char) =
object (self)
  inherit expression 2
	val mutable symbol : char = a_symbol
             
  method get_symbol = symbol

  method set_symbol (receivedSymbol : char) = 
  	symbol <- receivedSymbol;
                    
  method to_string = Printf.sprintf "%c" symbol      
end
 *)

class operation =
object(self)
  inherit exponent as super

    method string_of_operation (a_operation : operation_type) =
      super#string_of_exponent (a_operation.l) ^
        a_operation.symbol ^
          super#string_of_exponent (a_operation.r);
 
end

