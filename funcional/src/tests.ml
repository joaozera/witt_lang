open NumberDefinitions;;
open MlpWittLangFunctional;;



(* MULTIPLICAÇÃO DE 3 COM 5 *)

let multiplication_3_x_5 = multiplication new3 cinco;;

let multiplication_3_x_5_result = resolve(multiplication_3_x_5);;

let multiplication_3_x_5_str = tree_2_str(multiplication_3_x_5_result);;

Printf.printf "\nO resultado da multiplicacao de tres com cinco eh:\n\n %s \n\n" multiplication_3_x_5_str;;

let my_map = List.map is_operator (explode multiplication_3_x_5_str);;

let my_result_multiplication = string_of_int(soma_lista(my_map));;

Printf.printf "\nO resultado da multiplicacao de tres com cinco eh:\n\n %s \n\n" my_result_multiplication;;



(* SOMA DE 3 COM 5*)

let sum_3_5 = sum tres cinco;;

let sum_3_5_result = resolve(sum_3_5);;

let sum_3_5_str = tree_2_str(sum_3_5_result);;

Printf.printf "\nO resultado da soma de tres com cinco eh:\n\n %s \n\n" sum_3_5_str;;

let sum_3_5_list = List.map is_operator (explode sum_3_5_str);;

let sum_3_5_arabic = string_of_int(soma_lista(sum_3_5_list));;

Printf.printf "\nO resultado da soma de tres com cinco eh:\n\n %s \n\n" sum_3_5_arabic;;

(*
  let expr = multiplication(sum(tres, dos), sum(dos, dos));;
  
  let expr_result = resolve(expr);;
  
  let expr_str = tree_2_str(expr_result);;
  
    Printf.printf "\nO resultado da soma de tres com cinco eh:\n\n %s \n\n" sum_3_x_5_str;;
 *)

(*
let my_arit_op = Sum;;
let my_term = X;;
let my_arit_expr = Num 1;;
let my_arit_expr_2 = AE {left= my_arit_expr; oper= my_arit_op; right= my_arit_expr};;
let my_right_expoent = UN cinco;;
let my_left_expoent = UN cinco;;
let my_operation = {l= my_left_expoent; symbol= "O"; r= my_right_expoent};;
let my_expression = Application (Op my_operation, Term my_term);;
let my_expression_2 = Application (Application (Op my_operation, Op my_operation), Term my_term);;
let my_expression_3 = tree_expand(my_expression);;
   *)

(* soma 3 + 2 *)
(*let my_sum = tree_implode(tree_expand(Application(Application(Op {l= UN [One; One; One]; symbol= "O"; r = UN [One]}, Op {l= UN [One; One]; symbol= "O"; r= UN [One]}), Term my_term)));;*)

  (*
let my_expression_test_implode = Application(Op my_operation, Application(Op my_operation, Op my_operation)) ;;
let my_expression_test_implode_result = tree_implode(my_expression_test_implode);;

let resultado = tree_implode(my_expression_3);;
(* TESTES *)

let my_arit_expr_str = arith_expr_2_str(my_arit_expr);;
let my_arit_expr_2_str = arith_expr_2_str(my_arit_expr_2);;
let my_arit_op_str = arith_op_2_str(my_arit_op);;

let my_expression_str = tree_2_str(my_expression);;
let my_expression_str_2 = tree_2_str(my_expression_2);;
let my_expression_str_3 = tree_2_str(my_expression_3);;
let my_expression_test_implode_str = tree_2_str(my_expression_test_implode);;
let my_expression_test_implode_result_str = tree_2_str(my_expression_test_implode_result);;
let resultado_str = tree_2_str(resultado);;
let my_sum_str = tree_2_str(my_sum);;
 *)

(*
  Printf.printf "%s" my_arit_expr_2_str;;

  Printf.printf "\n\nexpressaozinha TOPPPPP \n\n %s" my_expression_str;;

    Printf.printf "\n\nexpressaozinha TOPPP expandida \n\n %s\n" my_expression_str_3;;

      Printf.printf "\n\nexpressaozinha pra testar a implosao \n\n %s\n" my_expression_test_implode_str;;

        Printf.printf "\n\nexpressaozinha pra testar a implosao rsultado \n\n %s\n" my_expression_test_implode_result_str;;

          Printf.printf "\n\nexpressaozinha pra testar a implosao rsultado \n\n %s\n" resultado_str;;

            Printf.printf "\n\n2 + 3, o famoso CINCAO \n\n %s\n" my_sum_str;;
 *)






(*
let rec parseInput (inputExpression : string, p1 : char, p2 : arithmeticGoods):
    match inputExpression(0) with
        | '(' -> push('(', p1) and parseInput(inputExpression )
        | ')' -> p1.pop and p2.top and p2.top and p2.top and p2.push and parseInput( )
        | _ -> p2.push and parseInput ( )



(*let Ae1: arithmeticExpression =(2, Mult, 3)*)

              (*printfn "ae1 = " Ae1 *)
*)
