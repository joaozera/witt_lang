(*

Trabalho Final da Disciplina de Modelos de Linguagens de Programação

Ciência da Computação - UFRGS - 2017/2

Alunos:
- João Marcos Flach - 242249
- Renan Luigi Martins Guarese - 193410

Interpretador da Linguagem da Teoria Geral das Operações

Implementação Funcional

*)

(* DEFINIÇÕES *)

open Types;;


(* FUNÇÕES DE STRING *)


let arith_op_2_str (my_arith_op) =
  match my_arith_op with
  | Sum -> "+"
  | Diff -> "-"
  | Mult -> "*"

let rec arith_expr_2_str (my_arith_expr) =
  match my_arith_expr with
  | Num lala -> string_of_int(lala)
  | AE lala -> arith_expr_2_str (lala.left) ^ arith_op_2_str (lala.oper) ^ arith_expr_2_str (lala.right)


let unarySymbol_2_str(myUnarySymbol) = "`"

let  unaryNumber_2_str(myUnaryNumber) =
  (String.concat "" (List.map (unarySymbol_2_str) myUnaryNumber))

let rec exponent_2_str(myExponent) =
  match myExponent with
  | AEs ae -> arith_expr_2_str(ae)
  | UN un -> unaryNumber_2_str(un)

let rec operation_2_str(myOperation) =  myOperation.symbol
  (*  "{" ^ exponent_2_str(myOperation.l) ^ "}"  ^ myOperation.symbol ^ "{" ^  exponent_2_str(myOperation.r) ^ "}" *)


(*let rec node_2_str(my_node) =
  match my_node with
  | Application (l, r) -> "   '   "
  | Op omega -> operation_2_str(omega)
  | Term cheese -> "X \n"*)

let rec tree_2_str(myExpression) =
  match myExpression with
  | Op omega -> operation_2_str(omega)
  | Application (left, right) -> " ' " ^ "[" ^  tree_2_str(left)^ "]"  ^ "["  ^  tree_2_str(right) ^ "]"
  | Term cheese -> "X" ;;



let rec expand_left_exponent(my_omega) =
  match my_omega.l with
  | AEs aes -> Op my_omega
  | UN unary ->  match unary with
                 | [] -> Op my_omega
                 | [x] -> Op my_omega
                 | head :: tail -> Application(expand_left_exponent({r = my_omega.r; symbol = my_omega.symbol; l =  UN [head]}), expand_left_exponent({r = my_omega.r; symbol = my_omega.symbol; l =  UN tail}))

let rec expand_right_exponent(my_omega) =
  match my_omega.r with
  | AEs aes -> Op my_omega
  | UN unary ->  match unary with
                 | [] -> expand_left_exponent(my_omega)
                 | [x] -> expand_left_exponent(my_omega)
                 | head :: tail -> Application(expand_right_exponent({l = my_omega.l; symbol = my_omega.symbol; r =  UN [head]}), expand_right_exponent({l = my_omega.l; symbol = my_omega.symbol; r =  UN tail}))



let rec tree_expand(myExpression) =
  match myExpression with
  | Application (left, right) -> Application(tree_expand(left), tree_expand(right))
  | Term cheese -> Term cheese
  | Op omega ->
     match omega.r with
     | AEs aes -> myExpression
     | UN unary -> match unary with
                   | [] -> expand_left_exponent(omega)
                   | [x] -> expand_left_exponent(omega)
                   | _ -> expand_right_exponent(omega)

(* Implementa a regra C *)
(* Verifica se tem alguma aplicação à direita de alguma aplicação *)
let rec tree_implode(myExpression) =
  match myExpression with
  | Term cheese -> Term cheese
  | Op omega -> Op omega
  | Application (left, right) ->
     match right with
       | Term cheese -> Application(tree_implode(left), right)
       | Op omega -> Application(tree_implode(left), right)
       | Application (right_left, right_right) -> tree_implode(Application(Application(tree_implode(left), tree_implode(right_left)), tree_implode(right_right)))

let resolve(myExpression) =
  tree_implode(tree_expand(myExpression));;


(*Exemplos de currying*)

let multiplication = function (num1) -> function (num2) ->
                                          Application
                                            (
                                              Op {l= UN num1; symbol= "O"; r = UN num2},
                                              Term X
                                            );;

let sum  = function (num1) -> function (num2) ->
                                Application
                                  (
                                    Application
                                      (
                                        Op {l= UN num1; symbol= "O"; r = UN [One]},
                                        Op {l= UN num2; symbol= "O"; r= UN [One]}
                                      ),
                                    Term X
                                  );;
  
 