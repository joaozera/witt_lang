open Types;;

(* DEFINIÇÃO DOS NÚMEROS UNÁRIOS *)



(* DEFINIÇÃO UM A UM *)

let um = [One];;
let dois = [One; One];;
let tres = [One; One; One];;
let quatro = [One; One; One; One];;
let cinco = [One; One; One; One; One];;



(* DEFINIÇÃO AUTOMÁTICA *)

let rec lastElement(l1) =
  match l1 with
  | [] -> []
  | [x] -> x
  | hd :: tl -> lastElement (tl)

let firstElement(l1) =
  match l1 with
  | [] -> []
  | hd :: tl -> hd

let rec findElement(l1, index) =
  if index = 1 then
      match l1 with
      | [] -> []
      | hd :: tl -> hd
  else
      match l1 with
      | [] -> []
      | hd :: tl -> findElement(tl, index-1)

let incrementUno(l1) =
  match l1 with
  | [] -> [One]
  | _ -> l1@[One]

let incrementDuo(l1) =
  match l1 with
  | [] -> [One; One]
  | _ -> l1@[One; One]

let increment(functionIncrement, functionPos, listNumbers) =
  functionIncrement(functionPos (listNumbers))

(* exemplo de função de alta ordem: *)

let rec makeNumberList(functionIncrement, l1, max) =
  if max = 0 then l1
  else
    makeNumberList(functionIncrement, l1@[increment(functionIncrement, lastElement, l1)], max-1)

    (* makeNumberList(functionIncrement, initial, l1@[  (fun fI, fP, lN -> fI(fP (lN))) (functionIncrement, lastElement, l1)   ], max-1) *)


let numeros = makeNumberList(incrementUno, [], 100);;

(* exemplo de função anônima: *)

let listaDeListasComFuncaoAnonymousGourmet =
  makeNumberList(
    (fun l1 -> match l1 with
                | [] -> [One]
                | _ -> l1@[One]
    ), [[One]], 2);;



let is_operator (charlinho) =
  if charlinho = 'O' then 1
  else 0

  
(*let listaCriada = increment (incrementUno, lastElement, listaNumeros);;*)


let new3 = findElement(numeros, 3);;



let rec soma_lista l1 =
  match l1 with
  | [] -> 0
  | h::t -> h + soma_lista(t);;

(*
Função de conversão de string para lista
retirada de: http://caml.inria.fr/pub/old_caml_site/FAQ/FAQ_EXPERT-eng.html#strings
com uma pequena modificação para pegar o tamanho da lista
*)
let explode s =
  let rec exp i l =
   if i < 0 then l else exp (i - 1) (s.[i] :: l) in
  exp (String.length s - 1) [];;



