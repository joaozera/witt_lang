
type terminal = X

type arithmeticOperator =
  | Sum
  | Diff
  | Mult 
(*//| Div //deixa o div de fora por ora*)

type arithmeticExpression =
 | Num of int
 | AE of
     {
       left : arithmeticExpression;
       oper : arithmeticOperator;
       right: arithmeticExpression
     };;


type unarySymbol = One

type unaryNumber = unarySymbol list

type exponent = 
 | AEs of arithmeticExpression
 | UN of unaryNumber

type operation =
  {
    l : exponent;
    symbol : string;
    r : exponent
  };;

(* expression é uma árvore de operações *)
type expression =
    | Op of operation
    | Application of expression * expression 
    | Term of terminal
