let rec tree_2_str(myExpression) =
    match myExpression with
    	  | Op omega -> operation_2_str(omega)
	  | Application apostrofo -> tree_2_str(apostrofo.left) ^ "\'" ^ tree_2_str(apostrofo.right)
	  | Terminal cheese -> "X"


let rec operation_2_str(myOperation) =
    exponent_2_str(myOperation.l) ^ char_2_str(myOperation.symbol) ^ exponent_2_str(myOperation.r)


let rec exponent_2_str(myExponent) =
    match myExponent with
    	  | AE ae -> arith_expr_2_str(myExponent)
	  | UN un -> unaryNumber_2_str(myExponent)


let  unaryNumber_2_str(myUnaryNumber) =
    (String.concat " " (List.map (unarySymbol_2_str) myUnaryNumber))

let unarySymbol_2_str(myUnarySymbol) =
    "1"

    