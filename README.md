# README #

### What is this repository for? ###

Wittgenstein's operation language.

### Links importantes ###

compilando ocaml
resumo: ocamlc gera bytecode e ocamlopt gera codigo nativo. usar ocamlopt
ocamlopt -o progprog module1.ml module2.ml
https://ocaml.org/learn/tutorials/compiling_ocaml_projects.html



strings em ocaml
https://caml.inria.fr/pub/docs/manual-ocaml/libref/String.html

### How do I get set up? ###

Install OCAML.

Pra não precisar colocar a senha do bitbucket toda vez, configurar uma ssh-key:
https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html#SetupanSSHkey-ssh2

Não esquecer que a origin deve estar associada ao SSH e não ao HTTP: https://confluence.atlassian.com/bitbucket/change-the-remote-url-to-your-repository-794212774.html

Install OPAM

wget https://raw.github.com/ocaml/opam/master/shell/opam_installer.sh -O - | sh -s /usr/local/bin

### Who do I talk to? ###

joaoflach@outlook.com