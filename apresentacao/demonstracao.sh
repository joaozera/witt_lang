#!/bin/bash

echo "Demonstração do trabalho de MLP"

echo "Witt Lang"

../oo/src/witt_lang_oo > out.txt

cat out.txt | sed 's/O/$\\Omega$/g' | sed 's/\[1$\\Omega$1\]/\[$\\Omega$\]/g' > input_tree.tex

echo "\begin{forest}" > tree1.tex

awk 'NR==1' input_tree.tex >> tree1.tex

echo "\end{forest}" >> tree1.tex

echo "\begin{forest}" > tree2.tex

awk 'NR==2' input_tree.tex >> tree2.tex

echo "\end{forest}" >> tree2.tex

echo "\begin{forest}" > tree3.tex

awk 'NR==3' input_tree.tex >> tree3.tex

echo "\end{forest}" >> tree3.tex


pdflatex demonstracao.tex

xdg-open demonstracao.pdf
