\select@language {brazilian}
\select@language {brazilian}
\select@language {brazilian}
\contentsline {chapter}{1~Introdu\IeC {\c c}\IeC {\~a}o}{3}{CHAPTER.1}
\contentsline {section}{\numberline {1.1}Sobre o Problema a ser Tratado}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Vis\IeC {\~a}o geral da linguagem}{5}{subsection.1.1.1}
\contentsline {chapter}{2~Regras da Linguagem de Wittgenstein}{6}{CHAPTER.2}
\contentsline {section}{\numberline {2.1}Regras}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Exemplo de multiplica\IeC {\c c}\IeC {\~a}o de 3 por 5}{7}{section.2.2}
\contentsline {chapter}{3~An\IeC {\'a}lise Cr\IeC {\'\i }tica}{9}{CHAPTER.3}
\contentsline {section}{\numberline {3.1}Tabela}{9}{section.3.1}
\contentsline {section}{\numberline {3.2}Discuss\IeC {\~a}o}{11}{section.3.2}
\contentsline {chapter}{4~Implementa\IeC {\c c}\IeC {\~a}o Funcional}{12}{CHAPTER.4}
\contentsline {section}{\numberline {4.1}Estruturas de Dados}{12}{section.4.1}
\contentsline {section}{\numberline {4.2}Implementa\IeC {\c c}\IeC {\~a}o das Regras}{12}{section.4.2}
\contentsline {section}{\numberline {4.3}Requisitos Funcionais}{12}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Elementos Imut\IeC {\'a}veis e Fun\IeC {\c c}\IeC {\~o}es Puras}{12}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Fun\IeC {\c c}\IeC {\~o}es An\IeC {\^o}nimas}{13}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Currying}{14}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Pattern Matching}{14}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Fun\IeC {\c c}\IeC {\~o}es de ordem superior implementadas}{14}{subsection.4.3.5}
\contentsline {subsection}{\numberline {4.3.6}Uso de fun\IeC {\c c}\IeC {\~o}es de ordem superior prontas}{14}{subsection.4.3.6}
\contentsline {subsection}{\numberline {4.3.7}Fun\IeC {\c c}\IeC {\~o}es como elementos de primeira de ordem}{15}{subsection.4.3.7}
\contentsline {subsection}{\numberline {4.3.8}Itera\IeC {\c c}\IeC {\~a}o via recurs\IeC {\~a}o em fun\IeC {\c c}\IeC {\~a}o de alta ordem}{15}{subsection.4.3.8}
\contentsline {chapter}{5~Implementa\IeC {\c c}\IeC {\~a}o Orientada a Objetos}{16}{CHAPTER.5}
\contentsline {section}{\numberline {5.1}Requisitos de Orienta\IeC {\c c}\IeC {\~a}o a Objetos}{16}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Defini\IeC {\c c}\IeC {\~a}o das Classes e Construtores}{16}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Destrutores}{17}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Namespaces}{17}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Encapsulamento}{18}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Heran\IeC {\c c}a M\IeC {\'u}ltipla}{18}{subsection.5.1.5}
\contentsline {subsection}{\numberline {5.1.6}Polimorfismo por Inclus\IeC {\~a}o}{19}{subsection.5.1.6}
\contentsline {subsection}{\numberline {5.1.7}Polimorfismo Param\IeC {\'e}trico}{19}{subsection.5.1.7}
\contentsline {subsection}{\numberline {5.1.8}Polimorfismo por Sobrecarga}{20}{subsection.5.1.8}
\contentsline {subsection}{\numberline {5.1.9}Delegates}{20}{subsection.5.1.9}
\contentsline {chapter}{6~Recursos Extras Utilizados}{21}{CHAPTER.6}
\contentsline {chapter}{7~Conclus\IeC {\~a}o}{22}{CHAPTER.7}
\contentsline {chapter}{\xspace {}Refer{\^e}ncias}{23}{schapter.2}
\contentsfinish 
